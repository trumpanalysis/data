# stream twitter from London
from tweepy.streaming import  StreamListener #listen to the tweets
from tweepy import OAuthHandler #authenticating the credenials
from tweepy import Stream
import json
import couchdb
import jma as twitter_credentials

# class for streamming and processing tweets
class TwitterStreamer():
    def stream_tweets(self, hash_tag_list):
        listener = MyListener()
        auth = OAuthHandler(twitter_credentials.consumer_key, twitter_credentials.consumer_secret)
        auth.set_access_token(twitter_credentials.access_token, twitter_credentials.access_token_secret)

        stream = Stream(auth, listener, tweet_mode='extended')
#filter the trump tweets in Texas in English
        stream.filter(track = hash_tag_list,languages=['en'],locations=[-0.4524,51.376,0.1813,51.6417])


# class print tweets
class MyListener(StreamListener): # class inherit from StreamListener
   #construtor
    def __init__(self):
        self.server = couchdb.Server("http://localhost:5984/")
        self.db = self.server['london_db']
        self.count = 0

    def on_status(self, status):
        try:
            text = status.extended_tweet["full_text"]
        except AttributeError:
            text = status.text

    # override
    def on_data(self,data):
        if(self.count == 50000):
            return False
        try:
            tweet = json.loads(data)
            try:
                self.db[str(tweet['id'])] = {'User_id':tweet['user']['id'],'Time':tweet['created_at'],'Text':tweet['extended_tweet']['full_text']
                                         ,'coordinates':tweet['coordinates'],'retweet_counts':tweet['retweet_count'],'favorite_count':tweet['favorite_count']}
            except:
                self.db[str(tweet['id'])] = {'User_id':tweet['user']['id'],'Time':tweet['created_at'],'Text':tweet['text']
                                         ,'coordinates':tweet['coordinates'],'retweet_counts':tweet['retweet_count'],'favorite_count':tweet['favorite_count']}
            self.count = self.count + 1
            return True
        except BaseException as e:
            print("Error on data : %s" % str(e))
            return True

    def on_error(self, status):
        #retrun False on_data method in case rate limit happens
        if status == 420:
            return False
        print(status)

#start running the program
if __name__ =='__main__':
    hash_tag_list = ["Donald Trump"]
    twitter_streamer = TwitterStreamer()
    twitter_streamer.stream_tweets(hash_tag_list)
