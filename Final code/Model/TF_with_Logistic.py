import csv
import numpy as np
import pandas as pd

train_data = []
with open("small_2train.csv","r", encoding='utf-8', errors='ignore') as f:
    reader = csv.reader(f)
    count = 0
    for row in reader:
        train_data.append(row)

train_text_x = []
train_text_y = []
for each in train_data:
    train_text_x.append(each[5])
    train_text_y.append(each[0])

total = train_text_x

import nltk
from collections import Counter
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import string

print("=============START PRE-PROCESSING=============")
def preprocessing(total):
    processed_docs = []
    total_voc = []
    count = 0
    percent = 10
    stemmer = nltk.stem.PorterStemmer()
    for raw in total:
        normal_doc = []
        temp = word_tokenize(raw)
        for each in range(len(temp)):
            if(not temp[each].isalpha()):
                continue
            stem_word = temp[each].lower()
            if(stem_word in stopwords.words('english')):
                continue
            if(stem_word=='' or stem_word=='“'or stem_word=='”' or stem_word=='’'
                or stem_word=='…' or ('http' in stem_word) or stem_word=='rt'or stem_word=="trump"
              or stem_word=="donald" or stem_word=="president" or stem_word=="realdonaldtrump"):
                continue
            normal_doc.append(stem_word)
            total_voc.append(stem_word)
        normal_doc = list(filter(None, normal_doc))
        processed_docs.append(normal_doc)
        count+=1
        if(int(count*100/len(total))==percent):
            print(".",end="")
            percent+=10
    return processed_docs,total_voc

processed_docs,total_voc = preprocessing(total)

def get_BOW(text):
    BOW = {}
    for word in text:
        BOW[word] = BOW.get(word,0) + 1
    return BOW

texts = []
for text in processed_docs:
    texts.append(get_BOW(text))
print("")
print("Done...")
print("")
print("=============START TF-IDF Feature SELECTION=============")
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

vectorizer = DictVectorizer()
brown_matrix = vectorizer.fit_transform(texts)

transformer = TfidfTransformer(smooth_idf=True,norm=None)
brown_matrix_tfidf = transformer.fit_transform(brown_matrix)

from sklearn.decomposition import TruncatedSVD

svd = TruncatedSVD(n_components=2000)
brown_matrix_lowrank = svd.fit_transform(brown_matrix_tfidf)

to = pd.DataFrame(brown_matrix_lowrank)

# test = to[:len(ptext)]
train = to
print("Done...")
print("")
print("=============START TRAINING MODEL=============")
from sklearn import linear_model
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(train, train_text_y,
                                                    test_size=0.3,
                                                    random_state=12)
parameter = {'penalty':['l1','l2'], 'tol':[1e-3, 1e-4], 'multi_class':['ovr'],'max_iter':[100,110]}
logistic = GridSearchCV(linear_model.LogisticRegression(), parameter, cv=10)
logistic.fit(X_train, y_train)

print("Best score: ", logistic.best_score_)
print("Test score: ",logistic.score(X_test, y_test))
print("Done...")
print("")
print("==================================OUTPUT PARAMETER SCORES==============================")
logistic.cv_results_['params']
logistic.cv_results_['mean_test_score']
for index in range(len(logistic.cv_results_['params'])):
    print("score: ",logistic.cv_results_['mean_test_score'][index],' with parameter: ',logistic.cv_results_['params'][index])
# print("")
# print("=============START PREDICTION=============")
# predictions = logistic.predict(test)
# result = pd.DataFrame({'tweet_id': tweet_id,'User_id':user_id,'Time':time,'City':city,'retweet_count':retweet_counts,
#                        'favorite_count':favorite_count,'Lable':predictions.astype(np.int32)})
# result.to_csv("TF_IDF_Logistic_result.csv", index=False)
# print("Done...")
# print("")
# positive_word = []
# negative_word = []
# test_words = processed_docs[:len(ptext)]
# for index in range(len(predictions)):
#     if(predictions[index]=='0'):
#         negative_word.extend(test_words[index])
#     else:
#         positive_word.extend(test_words[index])

# positive = Counter(positive_word).most_common(150)
# negative = Counter(negative_word).most_common(150)

# print("generating most frequency words")
# p_writer = csv.writer(open('TF_Positive_words.csv','w',encoding='utf-8',errors='ignore', newline=''))
# n_writer = csv.writer(open('TF_Negative_words.csv','w',encoding='utf-8',errors='ignore', newline=''))
# p_writer.writerow(["Words","Count"])
# n_writer.writerow(["Words","Count"])

# for word in positive:
#     p_writer.writerow([word[0], word[1]])
# for word in negative:
#     n_writer.writerow([word[0], word[1]])
# print("Done...")
