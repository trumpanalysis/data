import csv
import numpy as np
import pandas as pd

train_data = []
with open("small_train.csv","r", encoding='utf-8', errors='ignore') as f:
    reader = csv.reader(f)
    count = 0
    for row in reader:
        train_data.append(row)

train_text_x = []
train_text_y = []
for each in train_data:
    train_text_x.append(each[5])
    train_text_y.append(each[0])

total = train_text_x

import nltk
from collections import Counter
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import string

print("=============START PRE-PROCESSING=============")
def preprocessing(total):
    processed_docs = []
    total_voc = []
    count = 0
    percent = 10
    stemmer = nltk.stem.PorterStemmer()
    for raw in total:
        normal_doc = []
        temp = word_tokenize(raw)
        for each in range(len(temp)):
            if(not temp[each].isalpha()):
                continue
            stem_word = temp[each].lower()
            if(stem_word in stopwords.words('english')):
                continue
            if(stem_word=='' or stem_word=='“'or stem_word=='”' or stem_word=='’'
                or stem_word=='…' or ('http' in stem_word) or stem_word=='rt'or stem_word=="trump"
              or stem_word=="donald" or stem_word=="president" or stem_word=="realdonaldtrump"):
                continue
            normal_doc.append(stem_word)
            total_voc.append(stem_word)
        normal_doc = list(filter(None, normal_doc))
        processed_docs.append(normal_doc)
        count+=1
        if(int(count*100/len(total))==percent):
            print(".",end="")
            percent+=10
    return processed_docs,total_voc

processed_docs,total_voc = preprocessing(total)

def get_BOW(text):
    BOW = {}
    for word in text:
        BOW[word] = BOW.get(word,0) + 1
    return BOW

texts = []
for text in processed_docs:
    texts.append(get_BOW(text))
print("")
print("Done...")
print("")
print("=============START TF-IDF Feature SELECTION=============")
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

vectorizer = DictVectorizer()
brown_matrix = vectorizer.fit_transform(texts)

transformer = TfidfTransformer(smooth_idf=True,norm=None)
brown_matrix_tfidf = transformer.fit_transform(brown_matrix)

from sklearn.decomposition import TruncatedSVD

svd = TruncatedSVD(n_components=2000)
brown_matrix_lowrank = svd.fit_transform(brown_matrix_tfidf)

to = pd.DataFrame(brown_matrix_lowrank)

# test = to[:len(ptext)]
train = to
print("Done...")
print("")

print("=============START TRAINING MODEL=============")
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split

tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
X_train, X_test, y_train, y_test = train_test_split(train, train_text_y,
                                                    test_size=0.3,
                                                    random_state=12)
parameters = {}
SVM_model = GridSearchCV(SVC(gamma='scale'),param_grid=tuned_parameters,cv=10)
SVM_model.fit(X_train,y_train)
print("Best score: ", SVM_model.best_score_)
print("Test score: ",SVM_model.score(X_test, y_test))
print("Done...")
print("")
print("==================================OUTPUT PARAMETER SCORES==============================")
SVM_model.cv_results_['params']
SVM_model.cv_results_['mean_test_score']
for index in range(len(SVM_model.cv_results_['params'])):
    print("score: ",SVM_model.cv_results_['mean_test_score'][index],' with parameter: ',SVM_model.cv_results_['params'][index])
