# README #

This is the project using:       
Three feature selections: 1. POS tag 2. Unigram 3. TF-IDF               
with Four machine learning: 1. SVM 2. Logistic Regression 3. Random Forest 4. Naive Bayes              
and a Deep learning model.

### Scrawler? ###
Scrawler is the code that crawling the streaming tweet from different locations

### Model? ###
This is the folder contains all our models.                      
sentiment_checkpoint.keras is our neural network model check point which contains our current training status after 10 epochs.

### projectkey.pem? ###
This is the key to access our Virtual Machine
