from tweepy.streaming import  StreamListener #listen to the tweets
from tweepy import OAuthHandler #authenticating the credenials
from tweepy import Stream
from tweepy import API
from tweepy import Cursor
# get donald trump tweet timeline
import couchdb


import kliu as twitter_credentials
#Tweeter authenticater
class TwitterAuthenticator():
    def authenticate_twitter_app(self):
        auth = OAuthHandler(twitter_credentials.consumer_key, twitter_credentials.consumer_secret)
        auth.set_access_token(twitter_credentials.access_token, twitter_credentials.access_token_secret)
        return auth
# Tweet client
class TweetClient():
    #construtor
    def __init__(self, twitter_user ):
        self.server = couchdb.Server("http://localhost:5984/")
        self.db = self.server['test']
        self.id = 1
        # make this can stream tweet
        self.autu = TwitterAuthenticator().authenticate_twitter_app()
        self.tweet_client = API(self.autu)
        self.twitter_user = twitter_user

# get the user timeline tweets
    def get_user_timeline_tweets(self, numberOfTweets):
        try:
            tweets = []
            for tweet in Cursor(self.tweet_client.user_timeline, id = self.twitter_user).items(numberOfTweets):
                tweets.append(tweet)
                # # str.format
                # strTime = '{:%Y-%m-%d %H:%M:%S}'.format(tweet.created_at)
                # self.db[str(self.id)] = {'id':tweet.id, 'time': strTime,'text':tweet.text,
                #                          'favorite_count':tweet.favorite_count
                #                          ,'retweet_count':tweet.retweet_count}
                # self.id = self.id + 1
                print(tweet)
            return tweets
        except BaseException as e:
            print("Error on data : %s" % str(e))
            return True

    def on_error(self, status):
        #retrun False on_data method in case rate limit happens
        if status == 420:
            return False
        print(status)

# class print tweets
class TweeterListener(StreamListener): # class inherit from StreamListener
   #construtor
    def __init__(self, fetched_tweets_filename):
        self.fetched_tweets_filename = fetched_tweets_filename

    # override
    def on_data(self,data):
        try:
            print(data)
            with open (self.fetched_tweets_filename,'a') as tf:
                tf.write(data)
            return True
        except BaseException as e:
            print("Error on data : %s" % str(e))
            return True

    def on_error(self, status):
        #retrun False on_data method in case rate limit happens
        if status == 420:
            return False
        print(status)

#start running the program
if __name__ =='__main__':
    twitter_client = TweetClient('realDonaldTrump')
    #get the latest tweet from donald trump
    twitter_client.get_user_timeline_tweets(3500)
